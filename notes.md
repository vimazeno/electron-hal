# TODO

* remplacer les accesseurs tableau et utiliser les attirbuts
* normaliser l'import de module (config)
* tag > build

* requests from me to other hal
  * form search by api / pre filled / customizable
  * promises / spinner

* jeter un oeil sur
  * l'export bibTeX
  * l'import sword    
  * https://aster-community.teradata.com/community/learn-aster/blog/2015/06/21/data-science--fuzzy-matching-and-similarity-measures
  * https://github.com/ckreibich/scholar.py
  * https://github.com/dissemin/zotero
  * https://github.com/dissemin/dissemin

# HAL

* orcid pas dispo dans l'api

* pager marche pas

* pas de critère unifié par auteur (à checker dans une interface perso de hal)

* problème de la mise dans la bonne collection (LIMOS non LPC oui dans un premier temps (validation humaine?)
  * https://hal-clermont-univ.archives-ouvertes.fr/hal-01533675v2
    * collection non affichée dans les méta données

* Pascal Lafourcade (2 fois le même ...) > 43 articles cumulés hors sur le web de hal:
  * 40 - https://hal.archives-ouvertes.fr/search/index/?q=pascal+lafourcade&submit=&submitType_s=file&docType_s=ART+OR+COMM+OR+COUV+OR+OTHER+OR+OUV+OR+DOUV+OR+UNDEFINED+OR+REPORT+OR+THESE+OR+HDR+OR+LECTURE
  * 54 - https://hal.archives-ouvertes.fr/search/index/?q=pascal+lafourcade&docType_s=ART+OR+COMM+OR+COUV+OR+OTHER+OR+OUV+OR+DOUV+OR+UNDEFINED+OR+REPORT+OR+THESE+OR+HDR+OR+LECTURE
  * 55 - https://hal.archives-ouvertes.fr/search/index/?q=pascal+lafourcade

* Philippe Lacomme 6 entrées incoming toutes vides sans document ...
  * [alors que](https://hal.archives-ouvertes.fr/search/index/q/*/authFullName_s/Philippe+Lacomme)
    * comment peut on se retrouver si pauvre sur le même bonhommme?
    * comment peut on accéder au profil de ces auteurs? pas de cv (comme le dernier lafourcade)? pas de redirection vers un autre auteur  
    * comment fait on pour avoir accès à ses stats
      * Engelbert Mephu

* hal contient de t il de quoi faire des stats sur les citations d'un auteur
  * plugin wordpress > comment marche-t-il?



# JS

* reduce qui pare à 1 au lieu de 0

# Services

* http://dissem.in/
  * https://github.com/dissemin/dissemin/
  * http://dev.dissem.in/doc/
  * http://dev.dissem.in/api.html
  * http://dissem.in/api/search/

* dblp

* https://scholar.google.fr/
  * https://github.com/ckreibich/scholar.py

* https://www.researchgate.net/

# Workflow

me > hal all in collection > dlp all in hal > dissem.in all in hal
