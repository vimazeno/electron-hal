/******
* dblp
* http://dblp.org/search/publ/api?q=pascal+lafourcade&format=json
*/

module.exports = function () {
  $.ajax({
    url: "http://dblp.org/search/publ/api",
    dataType:'json',
    type: 'get',
    data: {
      "q": $("#author").val(),
      "format": "json"
    },
    success:function(data){
      //if(data.result.hits.length) {
      $('#dblp .docs tbody').html(
        data.result.hits.hit.reduce(function(docs, curr, ind, arr){
          if(ind == 1) {
            docs = nunjucks.render(
              'partials/doc_row_dblp.njk', {
                elt: arr[ind-1].info,
                regExp: new RegExp(config['collection'], 'g')
              }
            );
          }
          return docs += nunjucks.render(
            'partials/doc_row_dblp.njk', {
              elt: curr.info,
              regExp: new RegExp(config['collection'], 'g')
            }
          );
        })
      );
      $('#dblp .count').html(data.result.hits['@total'] + " docs found (" + data.result.hits.hit.length + " displayed)", "");
      //}
    }
  });
};
