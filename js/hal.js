/******
* hal
* http://api.archives-ouvertes.fr/search?q=pascal+lafourcade
* lacomme = authId_i:984788 -> 9
* lacomme = (authId_i:"984788" OR authId_i:"702108") -> 27
*/

config = require('./config');

module.exports = {

  search: function (query) {

    console.log('hal("' + query + '")');

    $('#hal .docs tbody').html(
      nunjucks.render('partials/doc_row_hal_progress.njk')
    )

    $.ajax({
      url: "http://api.archives-ouvertes.fr/search",
      dataType:'json',
      type: 'get',
      data: {
        "q": query,
        "fl": "title_s, fileMain_s,halId_s,authLastNameFirstName_s,collCode_s,docType_s,ePublicationDate_s,journalDate_s,producedDate_s,submittedDate_s,releasedDate_s,writingDate_s,authIdHal_i", // why? authIdHal_s,authOrcidIdExt_id",
        "sort": "submittedDate_s desc"
      },
      success:function(data){
        console.log(data);
        if(data.response.docs.length){
          console.log('update rows tpl');
          $('#hal .docs tbody').html(
            data.response.docs.reduce(function(docs, curr, ind, arr){
              if(ind == 1) {
                docs = nunjucks.render(
                  'partials/doc_row_hal.njk', {
                    elt: arr[ind-1],
                    regExp: new RegExp(config.collection, 'g')
                  }
                );
              }
              return docs += nunjucks.render(
                'partials/doc_row_hal.njk', {
                  elt: curr,
                  regExp: new RegExp(config.collection, 'g')
                }
              );
            })
          );
          $('#hal .count').html(data.response.numFound + " docs found (" + data.response.docs.length + " displayed)", "");
        }
        else {
          console.log('delete rows tpl');
          $('#hal .docs tbody').html("");
          $('#hal .count').html(" 0 docs found (0 displayed)");
        }
      }
    });
  }
};
