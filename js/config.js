var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)

var config = {};

/****
* read config file
*/
if (fs.existsSync('.hal.json')) {
  var str = fs.readFileSync('.hal.json','utf8');
  config = JSON.parse(str);
  if("firstname" in config) {
    $('#firstname').val(config.firstname);
  }

  if("lastname" in config) {
    $('#lastname').val(config.lastname);
  }

  if("collection" in config) {
    $('#collection').val(config.collection);
  }
}

config.update = function update() {
  config.firstname = $('#firstname').val();
  config.lastname = $('#lastname').val();
  config.collection = $('#collection').val();
  fs.writeFileSync('.hal.json', JSON.stringify(config, null, 4), 'utf8');
}

module.exports = config;
