module.exports = {
  author: function () {
    $.ajax({
      url: "http://api.archives-ouvertes.fr/ref/author",
      dataType:'json',
      type: 'get',
      data: {
        "q": config.firstname + " " + config.lastname,
        "fl": "firstName_s,lastName_s,idHal_i,idHal_s,valid_s,email_s", // to discover all fields -> "*",
      },
      success:function(data){

        if(data.response.docs.length) {

          $('#me .count').html(data.response.numFound + " authors found (" + data.response.docs.length + " displayed)", "");

          $('#me .authors tbody').html(
            data.response.docs.reduce(function(docs, curr, ind, arr){
              if(ind == 1) {

                docs = nunjucks.render(
                  'partials/me_tr_author.njk', {
                    elt: arr[ind-1]
                  }
                );
                docsByHalId(arr[ind-1].idHal_i);
              }
              docs += nunjucks.render(
                'partials/me_tr_author.njk', {
                  elt: curr
                }
              );
              docsByHalId(curr.idHal_i);
              return docs;

            })
          );

          $('#me .rest span').html(
            '<a target="_blank" href="' + this.url + '">' + decodeURIComponent(this.url) + '</a>'
          );
          /****
          * add to direct link to https://hal.archives-ouvertes.fr ;)
          * 40 - https://hal.archives-ouvertes.fr/search/index/?q=pascal+lafourcade&submit=&submitType_s=file&docType_s=ART+OR+COMM+OR+COUV+OR+OTHER+OR+OUV+OR+DOUV+OR+UNDEFINED+OR+REPORT+OR+THESE+OR+HDR+OR+LECTURE
          * 54 - https://hal.archives-ouvertes.fr/search/index/?q=pascal+lafourcade&docType_s=ART+OR+COMM+OR+COUV+OR+OTHER+OR+OUV+OR+DOUV+OR+UNDEFINED+OR+REPORT+OR+THESE+OR+HDR+OR+LECTURE
          * 55 - https://hal.archives-ouvertes.fr/search/index/?q=pascal+lafourcade
          */
        }
        else {
          $('#me .count').html("");
          $('#me .authors tbody').html("");
          $('#me .rest span').html("");
        }
      }
    });
  },
  halTabUpdateAndShow: function(hal_id) {
    hal.search("authIdHal_i:" + hal_id );
    $('#halQuery').val("authIdHal_i:" + hal_id);
    $('#search a[href="#hal"]').tab('show');
  }
};

function docsByHalId(hal_id) {
  /******
  * how many doc for this halId_i
  * https://hal.archives-ouvertes.fr/search/index/?qa[authIdHal_i][]=pascal-lafourcade&qa[text][]=&submit_advanced=Rechercher&rows=30
  */

  $('.numFound-' + hal_id).html(
    "<i class=\"fa fa-spinner fa-spin\" aria-hidden=\"true\"></i>"
  );
  $.ajax({
    url: "http://api.archives-ouvertes.fr/search",
    dataType:'json',
    type: 'get',
    data: {
      "q": "authIdHal_i:" +  hal_id
    },
    success:function(data){
      $('.numFound-' + hal_id).html(
        data.response.numFound
        + " <a href=\"https://hal.archives-ouvertes.fr/search/index/?qa[authIdHal_i][]=" + hal_id + "\"><i class=\"fa fa-link\"></i></a>"
        + " <a onClick=\"me.halTabUpdateAndShow(" + hal_id + ")\"><i class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"></i></a>"
      );
    },
    error:function(err) {
      //console.log("error");
      //reject( err );
    }
  });
}
