/******
* dissem.in
* http://dissem.in/api/search/?q=pascal+lafourcade
*/

module.exports = function () {
  $.ajax({
    url: "http://dissem.in/api/search",
    dataType:'json',
    type: 'get',
    data: {
      "q": $("#author").val(),
      "format": "json"
    },
    success:function(data){
      //if(data.papers.length) {
        $('#dissem-in .docs tbody').html(
          data.papers.reduce(function(docs, curr, ind, arr){
            if(ind == 1) {
              docs = nunjucks.render(
                'partials/doc_row_dissem_in.njk', {
                  elt: arr[ind-1],
                  regExp: new RegExp(config.collection, 'g')
                }
              );
            }
            return docs += nunjucks.render(
              'partials/doc_row_dissem_in.njk', {
                elt: curr,
                regExp: new RegExp(config.collection, 'g')
              }
            );
          })
        );
        $('#dissem-in .count').html(data.nb_results + " (" + data.papers.length + " displayed)", "");
      }
    //}
  });
};
