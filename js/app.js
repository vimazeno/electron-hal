/***
 * events
 */
require("./config");
hal = require("./hal");
dblp = require("./dblp");
dissem_in = require("./dissem_in");
me = require("./me");

me.author();

// open links with default browser in app
// see also https://github.com/electron/electron/issues/1344#issuecomment-171516636
var shell = require('electron').shell;

$(document).on('click', 'a[href^="http"]', function(event) {
  event.preventDefault();
  shell.openExternal(this.href);
});

$(document).on('blur', '.autosave input', function(event) {
  config.update();
  me.author();
});

$(document).on('keypress', '.autosave input', function(event) {
  if(event.which == 13) {
    config.update();
    me.author();
  }
});

$(document).on('click', '#halQueryBtn', function(event) {
  hal.search($('#halQuery').val());
});
